module.exports = {
  en: 'English',
  eu: 'Euskara',
  de: 'Deutsch',
  es: 'Español',
  gl: 'Galego',
  it: 'Italiano',
  ca: 'Català',
  fr: 'Francais',
  nb: 'Norwegian Bokmål',
  sk: 'Slovak',
  pl: 'Polski'
}
